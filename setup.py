#!/usr/bin/env python

from distutils.core import setup

setup(name='Mlox',
    version='0.6.2',
    url='https://github.com/mlox/mlox',
    packages=['modules'],
    scripts=['mlox'],
    data_files=[
        ('/usr/share/mlox/Resources', ['Resources/mlox.gif', 'Resources/mlox.ico', 'Resources/mlox.msg']),
    ]
 )